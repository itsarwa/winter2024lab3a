public class Application{
	
	
	public static void main(String[] args){
	
	Student s1= new Student( "Arwa", 80, "comp sci");
	Student s2= new Student( "Sarah", 100, "social science");
	
	
	System.out.println(s1.sayHi() + s1.yourProgram());
	//System.out.print(s1.yourProgram());
	
	System.out.println(s2.sayHi()+ s2.yourProgram());
	//System.out.print(s2.yourProgram());
	//Student.sayHi();
	
	Student[] section3 = new Student[3];
	section3[0]=s1;
	section3[1]=s2;
	section3[2]= new Student("Eric", 20, "engineering");
	
	//System.out.println(section3[0].name);
	System.out.println(section3[2].name + " " + section3[2].average + " " + section3[2].program);
	}
}