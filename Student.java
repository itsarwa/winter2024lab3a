public class Student{
	public String name;
	public int average;
	public String program;
	
	public Student(String n, int avg, String prog){
		name=n;
		average=avg;
		program=prog;
	}
	public String sayHi(){
		return "Hi " + name + "!";
	}
	public String yourProgram(){
		return " You're in " + program;
	}
}